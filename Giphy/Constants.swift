//
//  Constants.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation

struct Constants {
    static let baseURL = "https://api.giphy.com/v1/"
    static let apiKey = "32tM9IjU0YRgOnCq6zG0LHVfWyoaPSIX"

    struct Trending {
        static let APILimit = 25
        static let APIPage = 20
    }
}
