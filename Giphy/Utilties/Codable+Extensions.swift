//
//  Codable+Extensions.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation

extension String: Error {
}

extension Decodable {
    static func decode(with decoder: JSONDecoder = JSONDecoder(), from data: Data?) throws -> Self {
        guard let data = data else {
            throw "Empty response"
        }
        return try decoder.decode(Self.self, from: data)
    }

}
