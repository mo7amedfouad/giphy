//
//  Gif.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation


struct Gif: Codable {
    let id: String
    let title: String
    let images: Images
}

struct Images: Codable {
    let preview_gif: Image?
    let original: Image?
}

struct Image: Codable {
    let url: URL?
    let width: String?
    let height: String?
}
