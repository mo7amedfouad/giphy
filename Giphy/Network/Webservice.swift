//
//  Webservice.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation
import PromiseKit

protocol Executable {
    func execute<Result>(_ resource: Resource<Result>) -> Promise<Result>
}

struct Webservice: Executable {
    let session = URLSession.init(configuration: .default)

    func execute<Result>(_ resource: Resource<Result>) -> Promise<Result> {
        guard let url = resource.url else {
            return .init(error: "Bad URL")
        }
        let request = URLRequest(url: url)
        return session.dataTask(.promise, with: request).validate().compactMap(on: DispatchQueue.global(qos: .background)) { data, response in
            return resource.parser(data)
        }
    }
}

enum HTTPMethod: String {
    case GET, POST, PUT, DELETE
    var type: String {
        return self.rawValue
    }
}
