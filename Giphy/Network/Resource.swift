//
//  Resource.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation

struct Resource<Result> {
    public typealias Parser = ((Data?) -> Result?)

    let url: URL?
    let method: HTTPMethod
    let parser: Parser
    let headers: [String: String]

    init(path: String = "",
         baseURL: String = Constants.baseURL,
         method: HTTPMethod = .GET,
         queryParameters: [String: String]? = nil,
         headers: [String: String] = [:],
         parser: @escaping Parser) {
        self.url = Resource.makeURL(path: path, baseURL: baseURL, queryParameters: queryParameters)
        self.headers = headers
        self.parser = parser
        self.method = method
    }

    static func makeURL(path: String, baseURL: String, queryParameters: [String: String]? = nil) -> URL? {
        var urlComponents = URLComponents(string: baseURL)
        urlComponents?.path += path
        guard !(queryParameters?.isEmpty ?? true) else {
            return urlComponents?.url
        }
        urlComponents?.queryItems = queryParameters?.map({ queryParameter in URLQueryItem(name: queryParameter.key, value: queryParameter.value) })
        return urlComponents?.url
    }
}
