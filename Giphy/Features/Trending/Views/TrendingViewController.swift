//
//  TrendingViewController.swift
//  Giphy
//
//  Created by Mohamed Fouad on 5/31/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import UIKit

class TrendingViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    private(set) var trendingViewModel: TrendingListViewModel!

    static func initialize(trendingViewModel: TrendingListViewModel) -> TrendingViewController {
        let vc = TrendingViewController.instantiate(.trending)
        vc.trendingViewModel = trendingViewModel
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Trending Gifs"
        fetch()
    }

    func fetch() {
        trendingViewModel.fetchNext().done { [weak self] in
            self?.collectionView.reloadData()
        }.catch({ [weak self] (error) in
            let alert = UIAlertController(title: "Something went wrong",
                    message: error.localizedDescription,
                    preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self?.present(alert, animated: true)

        })
    }
}

extension TrendingViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trendingViewModel.previewViewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsViewModel = trendingViewModel.detailsViewModel(at: indexPath.item)
        let vc = GifDetailsViewController.initialize(gifDetailsViewModel: detailsViewModel)
        self.navigationController?.pushViewController(vc, animated: true)

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GifListingCollectionViewCell", for: indexPath) as? GifListingCollectionViewCell else {
            return UICollectionViewCell()
        }
        let gif = trendingViewModel.previewViewModels[indexPath.item]
        cell.viewModel = gif
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            fetch()
        }
    }

}


