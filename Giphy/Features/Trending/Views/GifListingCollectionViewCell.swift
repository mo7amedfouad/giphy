//
//  GifListingCollectionViewCell.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import UIKit
import Kingfisher

class GifListingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var gifPreviewImageView: UIImageView!
    var viewModel: GifPreviewViewModel? {
        didSet {
            gifPreviewImageView.kf.setImage(with: viewModel?.imageURL)
        }
    }
}
