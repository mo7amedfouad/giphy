//
//  TrendingList.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation

struct TrendingList: Codable {
    let data: [Gif]
    let pagination: pagination
}

struct pagination: Codable {
    let total_count: Int
    let count: Int
    let offset: Int
}

extension TrendingList {
    static func trending(offset: Int, limit: Int) -> Resource<TrendingList> {
        return Resource<TrendingList>.init(path: "gifs/trending",
                queryParameters: ["api_key": Constants.apiKey,
                                  "offset": String(offset),
                                  "limit": String(limit)],
                parser: {
                    return try? TrendingList.decode(from: $0)
                })
    }
}
