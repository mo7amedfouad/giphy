//
//  GifPreviewViewModel.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation

struct GifPreviewViewModel {
    let imageURL: URL?

    init(_ gif: Gif) {
        self.imageURL = gif.images.preview_gif?.url
    }
}
