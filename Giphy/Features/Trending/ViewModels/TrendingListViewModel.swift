//
//  TrendingListViewModel.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation
import PromiseKit

class TrendingListViewModel {
    private(set) var previewViewModels: [GifPreviewViewModel] = []
    private(set) var offset: Int = 0

    private let webService: Executable
    private var gifs: [Gif] = []

    init(webService: Executable) {
        self.webService = webService
    }

    func fetchNext() -> Promise<Void> {
        guard Constants.Trending.APIPage > (offset / 20) else {
            return Promise().asVoid()
        }
        let resource = TrendingList.trending(offset: offset, limit: Constants.Trending.APILimit)
        offset += Constants.Trending.APILimit
        return webService.execute(resource).then(updateData)
    }

    private func updateData(trendingList: TrendingList) -> Promise<Void> {
        gifs.append(contentsOf: trendingList.data)
        previewViewModels.append(contentsOf: trendingList.data.map(GifPreviewViewModel.init))
        return Promise().asVoid()
    }

    func detailsViewModel(at index: Int) -> GifDetailsViewModel {
        let gif = gifs[index]
        return GifDetailsViewModel(gif)
    }

    func reset() -> Promise<Void> {
        offset = 0
        gifs.removeAll()
        previewViewModels.removeAll()
        return Promise().asVoid()
    }

}
