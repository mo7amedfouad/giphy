//
//  GifDetailsViewController.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import UIKit
import Kingfisher

class GifDetailsViewController: UIViewController {

    @IBOutlet weak var gifImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    var gifDetailsViewModel: GifDetailsViewModel?

    static func initialize(gifDetailsViewModel: GifDetailsViewModel) -> GifDetailsViewController {
        let vc = GifDetailsViewController.instantiate(.details)
        vc.gifDetailsViewModel = gifDetailsViewModel
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.gifImageView.kf.setImage(with: gifDetailsViewModel?.imageURL)
        self.titleLabel.text = gifDetailsViewModel?.title
    }


}
