//
//  GifDetailsViewModel.swift
//  Giphy
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation

struct GifDetailsViewModel {
    let imageURL: URL?
    let title: String?

    init(_ gif: Gif) {
        self.imageURL = gif.images.original?.url
        self.title = gif.title
    }


}
