//
//  MockWebservice.swift
//  GiphyTests
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import Foundation
import PromiseKit

@testable import Giphy

class MockWebservice: Executable {

    func execute<Result>(_ resource: Resource<Result>) -> Promise<Result> {
        guard let url = resource.url else {
            return .init(error: "Bad URL")
        }
        let bundle = Bundle(for: type(of: self))
        guard url.absoluteString.contains("trending"), let path = bundle.path(forResource: "mock", ofType: "json") else {
            return .init(error: "Missing mocks")
        }

        let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        if let value = resource.parser(data) {
            return .value(value)
        } else {
            return .init(error: "Bad JSON")
        }


    }
}
