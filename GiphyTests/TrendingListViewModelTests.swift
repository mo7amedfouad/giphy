//
//  TrendingListViewModelTests.swift
//  GiphyTests
//
//  Created by Mohamed Fouad on 6/2/19.
//  Copyright © 2019 Mohamed Fouad. All rights reserved.
//

import XCTest
import PromiseKit

@testable import Giphy

class TrendingListViewModelTests: XCTestCase {

    let webservice = MockWebservice() // can be mocked for UI test cases

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInit() {
        let trendingViewModel = TrendingListViewModel(webService: webservice)
        XCTAssertEqual(trendingViewModel.offset, 0)
        XCTAssertEqual(trendingViewModel.previewViewModels.count, 0)
    }


    func testFetchNext() {
        let trendingViewModel = TrendingListViewModel(webService: webservice)
        let expectation = XCTestExpectation(description: "testFetchNext")
        trendingViewModel.fetchNext().done {
            XCTAssertEqual(trendingViewModel.offset, 25)
            XCTAssertEqual(trendingViewModel.previewViewModels.count, 25)
            expectation.fulfill()
        }.cauterize()
        wait(for: [expectation], timeout: 10.0)
    }

    func testPreviewViewModel() {
        let trendingViewModel = TrendingListViewModel(webService: webservice)
        let expectation = XCTestExpectation(description: "testPreviewViewModel")
        trendingViewModel.fetchNext().done {
            let previewViewModel = trendingViewModel.previewViewModels[0]
            XCTAssertEqual(previewViewModel.imageURL?.absoluteString, "https://media2.giphy.com/media/3ornjWylTTz6e0SHIs/giphy-preview.gif?cid=e82833895cf3993b4b754464554ad14c&rid=giphy-preview.gif")
            expectation.fulfill()
        }.cauterize()
        wait(for: [expectation], timeout: 10.0)
    }

    func testDetailsViewModel() {
        let trendingViewModel = TrendingListViewModel(webService: webservice)
        let expectation = XCTestExpectation(description: "testDetailsViewModel")
        trendingViewModel.fetchNext().done {
            let detailsViewModel = trendingViewModel.detailsViewModel(at: 0)
            XCTAssertEqual(detailsViewModel.imageURL?.absoluteString, "https://media2.giphy.com/media/3ornjWylTTz6e0SHIs/giphy.gif?cid=e82833895cf3993b4b754464554ad14c&rid=giphy.gif")
            XCTAssertEqual(detailsViewModel.title, "seth meyers drinking GIF by Late Night with Seth Meyers")
            expectation.fulfill()
        }.cauterize()
        wait(for: [expectation], timeout: 10.0)

    }

    func testReset() {
        let trendingViewModel = TrendingListViewModel(webService: webservice)
        let expectation = XCTestExpectation(description: "testReset")
        firstly {
            trendingViewModel.fetchNext()
        }.then {
            trendingViewModel.reset()
        }.done {
            XCTAssertEqual(trendingViewModel.offset, 0)
            XCTAssertEqual(trendingViewModel.previewViewModels.count, 0)
            expectation.fulfill()
        }.cauterize()
        wait(for: [expectation], timeout: 10.0)

    }


}
